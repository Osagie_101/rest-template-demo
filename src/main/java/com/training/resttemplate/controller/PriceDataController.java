package com.training.resttemplate.controller;

import com.training.resttemplate.entity.PriceDataWrapper;
import com.training.resttemplate.service.PriceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/vi/pricedata")
public class PriceDataController {

    @Autowired
    PriceDataService priceDataService;

    @GetMapping("{ticker}")
    public PriceDataWrapper getForTicker(@PathVariable String ticker) {
        return priceDataService.getForTicker(ticker);
    }

}
