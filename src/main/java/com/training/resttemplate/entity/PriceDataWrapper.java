package com.training.resttemplate.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

// this annotation means that if there's any fields in the JSON that aren't
// in this object then those fields will be ignored
@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceDataWrapper {

    private String ticker;

    // tell the JSON marshaller that this field in the JSON is called "price_data"
    @JsonProperty("price_data")
    private PriceData priceData;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public PriceData getPriceData() {
        return priceData;
    }

    public void setPriceData(PriceData priceData) {
        this.priceData = priceData;
    }

    @Override
    public String toString() {
        return "PriceDataWrapper{" +
                "stockTicker = " + ticker +
                ", priceData = " +  priceData +
                "}";
    }
}
