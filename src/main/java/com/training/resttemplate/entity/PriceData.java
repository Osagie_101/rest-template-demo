package com.training.resttemplate.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

// this annotation means that if there's any fields in the JSON that aren't
// in this object then those fields will be ignored
@JsonIgnoreProperties(ignoreUnknown = true)
public class PriceData {

    private List<Double> open;
    private List<Double> high;
    private List<Integer> volume;
    private List<Double> low;
    private List<Double> close;

    // tell the JSON marshaller about the format of the date strings in the JSON
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private List<Date> timestamp;

    public List<Double> getOpen() {
        return open;
    }

    public void setOpen(List<Double> open) {
        this.open = open;
    }

    public List<Double> getHigh() {
        return high;
    }

    public void setHigh(List<Double> high) {
        this.high = high;
    }

    public List<Integer> getVolume() {
        return volume;
    }

    public void setVolume(List<Integer> volume) {
        this.volume = volume;
    }

    public List<Double> getLow() {
        return low;
    }

    public void setLow(List<Double> low) {
        this.low = low;
    }

    public List<Double> getClose() {
        return close;
    }

    public void setClose(List<Double> close) {
        this.close = close;
    }

    public List<Date> getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(List<Date> timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "PriceData{" +
                "open=" + open +
                ", high=" + high +
                ", volume=" + volume +
                ", low=" + low +
                ", close=" + close +
                ", timestamp=" + timestamp +
                '}';
    }
}
