package com.training.resttemplate.repository;

import com.training.resttemplate.entity.PriceDataWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class PriceDataRepository {

    // get this variable from application.properties
    @Value("${price.data.endpoint.url}")
    private String priceDataEndpointUrl;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Retrieves the price data from cachedPriceData HTTP endpoint
     */
    public PriceDataWrapper getForTicker(String ticker) {
        // Append the query variable to the url from application.properties
        String urlWithParams = priceDataEndpointUrl + "?ticker=" + ticker;

        // Use the spring RestTemplate object to send a HTTP GET and parse the
        // returned JSON into a PriceDataWrapper object
        // NOTE: we have created the restTemplate Bean in RestTemplateApplication.java
        return restTemplate.getForObject(urlWithParams, PriceDataWrapper.class);
    }
}
